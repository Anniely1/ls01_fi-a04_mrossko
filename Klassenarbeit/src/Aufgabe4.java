public class Aufgabe4 {

    public static void main(String[] args) {
        int zahl = 5;
        System.out.print(romanDidget(zahl));
    }

    private static String romanDidget(int zahl) {
        switch (String.valueOf(zahl)) {
            case "1":
                return "I";
			case "5":
                return "V";
			case "10":
                return "X";
			case "50":
                return "L";
			case "100":
                return "C";
			case "500":
                return "D";
			case "1000":
                return "M";
		}
        return "?";
    }

    //hier soll Ihr Quelltext hin

}
