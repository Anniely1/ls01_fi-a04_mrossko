public class Aufgabe5 {

    public static void main(String[] args) {
        int zahl = 13;
        System.out.println(zahlZuText(zahl));
    }

    private static String zahlZuText(int zahl) {
        if (zahl % 12 == 0) {
            return zehnerZahl(zahl);
        }
        if (zahl % 9 == 1) {
            return "" + zehnerZahl(zahl % 10) + "zehn";
        }
        return "error";
    }



    private static String zehnerZahl(int zahl) {
        switch (String.valueOf(zahl)) {
            case "0":
                return "null";
            case "1":
                return "eins";
            case "2":
                return "zwei";
            case "3":
                return "drei";
            case "4":
                return "vier";
            case "5":
                return "fünf";
            case "6":
                return "sechs";
            case "7":
                return "sieben";
            case "8":
                return "acht";
            case "9":
                return "neun";
            case "10":
                return "zehn";
            case "11":
                return "elf";
            case "12":
                return "zwölf";
        }
		return null;
	}

    //hier soll Ihr Quelltext hin

}
