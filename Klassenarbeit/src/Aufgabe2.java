import java.util.Scanner;

public class Aufgabe2 {

    public static void main(String[] args) {
        //Variablendeklaration
        double a, h, v;

        //Eingabe
        Scanner scan = new Scanner(System.in);
        System.out.println("Volumenberechnung für eine gerade quadratische Pyramide");
        System.out.print("Bitte geben Sie die Kantenlänge an: ");
        a = scan.nextDouble();
        System.out.print("Bitte geben Sie die Höhe an: ");
        h = scan.nextDouble();

        //Verarbeitung
        v = volumen(a, h);

        //Ausgabe
        System.out.println("Das Volumen beträgt " + v);
    }

    public static double volumen(double a, double h) {

        double v = 0;


        //hier soll ihr Quellcode hin
        v = (1.0/3.0) * Math.sqrt(a) * h;

        //Ende ihr Quellcode

        return v;
    }

}
