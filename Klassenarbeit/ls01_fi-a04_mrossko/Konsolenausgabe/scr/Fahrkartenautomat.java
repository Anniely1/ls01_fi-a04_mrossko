import java.util.Scanner;

class Fahrkartenautomat {
    static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args) {


        double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        double eingezahlterGesamtbetrag = fahrkartenbezahlen(zuZahlenderBetrag);


        fahrkartenAusgeben();
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");
    }

    public static double fahrkartenbestellungErfassen() {
        System.out.print("Zu zahlender Betrag (EURO): ");
        double zuZahlenderBetrag = tastatur.nextDouble();
        zuZahlenderBetrag = validatePrice(zuZahlenderBetrag);
        System.out.println("Wie viele Karten wollen Sie kaufen?");
        int kartenAnzahl = tastatur.nextInt();
        kartenAnzahl = validateNumber(kartenAnzahl);
        return Math.round((zuZahlenderBetrag * kartenAnzahl) * 100.00) / 100.00;
    }

    private static double validatePrice(double zuZahlenderBetrag) {

        while (zuZahlenderBetrag <= 0) {
            System.out.println("Ungültiger Betrag, bitte geben Sie einen gültigen Betrag ein");
            zuZahlenderBetrag = tastatur.nextDouble();
        }
        return zuZahlenderBetrag;
    }

    private static int validateNumber(int kartenAnzahl) {
        while ((kartenAnzahl > 10) || (kartenAnzahl < 0)) {
            System.out.println("Ungültige Fahrkartenanzahl, es müssen mindestens 1 aber maximal 10 Karten gekauftwerden");
            kartenAnzahl = tastatur.nextInt();
        }
        return kartenAnzahl;

    }

    public static double fahrkartenbezahlen(double zuZahlen) {
        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlen) {
            System.out.printf("%s%.2f%c\n", "Noch zu zahlen: ", Math.round((zuZahlen - eingezahlterGesamtbetrag) * 100.00) / 100.00, '€');
            System.out.print(" Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += Math.round((eingeworfeneMünze * 100.00) / 100.00);
        }
        return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben() {
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    // Rückgeldberechnung und -Ausgabe
    // -------------------------------
    private static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        double rückgabebetrag = Math.round((eingezahlterGesamtbetrag - zuZahlenderBetrag) * 100.00) / 100.00;
        if (rückgabebetrag > 0.0) {
            System.out.printf("%s%.2f%s\n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
                rückgabebetrag = Math.round((rückgabebetrag) * 100.00) / 100.00;


            }
            while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
                rückgabebetrag = Math.round((rückgabebetrag) * 100.00) / 100.00;

            }
            while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
                rückgabebetrag = Math.round((rückgabebetrag) * 100.00) / 100.00;

            }
            while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
                rückgabebetrag = Math.round((rückgabebetrag) * 100.00) / 100.00;

            }
            while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
                rückgabebetrag = Math.round((rückgabebetrag) * 100.00) / 100.00;

            }
            while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
                rückgabebetrag = Math.round((rückgabebetrag) * 100.00) / 100.00;

            }
        }
    }
}