package for_schleifen;

import java.util.Scanner;

public class Fakultät {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int fakultät = 1;
        while (n > 0) {
            fakultät = fakultät*n;
            n--;
        }
        System.out.println(fakultät);
    }
}
