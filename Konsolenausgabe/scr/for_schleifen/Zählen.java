package for_schleifen;

import java.util.Scanner;

public class Zählen {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int i = 1;
        while (i <= n) {
            System.out.print(" " + i);
            i++;
        }
        i--;
        System.out.println();
        while (i > 0) {
            System.out.print(" " + i);
            i--;
        }
    }
}
