package Auswahlstrukturen;

import java.util.Scanner;

public class Schaltjahr {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        boolean isLeapYear= false;
        if (year%4==0){
            isLeapYear= true;
        }
        if(year%100==0){
            isLeapYear=false;
        }
        if(year%400==0){
            isLeapYear=true;
        }
System.out.println(isLeapYear);
    }
}
