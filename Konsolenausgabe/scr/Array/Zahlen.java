package Array;

//Schreiben Sie ein Programm „Zahlen“, in welchem ein ganzzahliges Array der Länge 10
//        deklariert wird. Anschließend wird das Array mittels Schleife mit den Zahlen von 0 bis 9
//        gefüllt. Zum Schluss geben Sie die Elemente des Arrays wiederum mit einer Schleife auf der
//        Konsole aus.
public class Zahlen {
    public static void main(String[] args) {
        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i;
        }
        for (int j = 0; j <= 9; j++) {
            System.out.println(arr[j]);
        }
    }
}
