package Array;
//Das zu schreibende Programm „UngeradeZahlen“ ist ähnlich der Aufgabe 1. Sie deklarieren
//        wiederum ein Array mit 10 Ganzzahlen. Danach füllen Sie es mit den ungeraden Zahlen von 1 bis 19
//        und geben den Inhalt des Arrays über die Konsole aus (Verwenden Sie Schleifen!).

public class UngeradeZahlen {
    public static void main(String[] args) {
        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 2 * i + 1;

        }
        for (int j = 0; j < arr.length; j++) {
            System.out.println(arr[j]);
        }
    }
}
