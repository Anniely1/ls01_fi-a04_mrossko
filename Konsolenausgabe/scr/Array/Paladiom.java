package Array;

import java.util.Scanner;

//Im Programm „Palindrom“ werden über die Tastatur 5 Zeichen eingelesen und in einem
//        geeigneten Array gespeichert. Ist dies geschehen, wird der Arrayinhalt in umgekehrter
//        Reihenfolge (also von hinten nach vorn) auf der Konsole ausgegeben.
public class Paladiom {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arr = new int[5];
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Bitte geben Sie eine Ganze Zahl ein");
            arr[i] = sc.nextInt();
        }
        for (int j = arr.length-1; j>=0; j--) {
            System.out.println(arr[j]);
        }
    }
}
