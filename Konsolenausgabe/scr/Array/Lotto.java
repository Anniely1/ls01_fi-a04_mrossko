package Array;

//Jetzt wird Lotto gespielt. In der Klasse „Lotto“ gibt es ein ganzzahliges Array, welches 6 Lottozahlen
//        von 1 bis 49 aufnehmen kann. Konkret sind das die Zahlen 3, 7, 12, 18, 37 und 42. Tragen Sie diese im
//        Quellcode fest ein.
//        a) Geben Sie zunächst schleifenbasiert den Inhalt des Arrays in folgender Form aus:
//        [ 3 7 12 18 37 42 ]
//        b) Prüfen Sie nun nacheinander, ob die Zahlen 12 bzw. 13 in der Lottoziehung vorkommen.
//        Geben Sie nach der Prüfung aus:
//        Die Zahl 12 ist in der Ziehung enthalten.
//        Die Zahl 13 ist nicht in der Ziehung enthalten.
public class Lotto {
    public static void main(String[] args) {
        int[] arr = {3, 7, 12, 18, 37, 42};
        System.out.print("[\t");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
        System.out.print("]\n");
        output(12, validate(12, arr));
        output(13, validate(13, arr));
    }

    private static boolean validate(int number, int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == number) {
                return true;
            }
        }
        return false;
    }

    private static void output(int number, boolean included) {
        if (included) {
            System.out.println("Die Zahl " + number + " ist in der Ziehung enthalten.");
        } else {
            System.out.println("Die Zahl " + number + " ist nicht in der Ziehung enthalten.");

        }

    }
}

